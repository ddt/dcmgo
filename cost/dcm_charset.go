package cost

const (
	ISO_IR_6 = "ISO-IR 6"
	ASCII = "US-ASCII"
	ISO_8859_1 = "ISO-8859-1"
	Big5     = "Big5"
	GB18030  = "GB18030"
	GBK  = "GBK"
	JIIS0201  = "EUC-JP"
	JIIS0208  = "EUC-JP"
	JIIS0212  = "EUC-JP"
	UTF8 = "UTF-8"
	UTF16 = "UTF-16"
)
