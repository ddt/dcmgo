package cost

const (
	ImplictVREndian               = "1.2.840.10008.1.2"
	ExplictVRLittleEndian         = "1.2.840.10008.1.2.1"
	ExplictVRBigEndian            = "1.2.840.10008.1.2.2"
	DeflatedExplictVRLittleEndian = "1.2.840.10008.1.2.1.99"

	JPEG8BitImageCompression  = "1.2.840.10008.1.2.4.50"
	JPEG12BitImageCompression = "1.2.840.10008.1.2.4.51"
	JPEGLossless              = "1.2.840.10008.1.2.4.57"
	JPEGLosslessFirstOrder    = "1.2.840.10008.1.2.4.70"
	JPEGLSLossless            = "1.2.840.10008.1.2.4.80"
	JPEGLSLossy               = "1.2.840.10008.1.2.4.81"
	JPEG2KLossless            = "1.2.840.10008.1.2.4.90"
	JPEG2K                    = "1.2.840.10008.1.2.4.91"
	JPEG2KMultiLossless       = "1.2.840.10008.1.2.4.92"
	JPEG2KMulti               = "1.2.840.10008.1.2.4.93"

	JPIP        = "1.2.840.10008.1.2.4.94"
	JPIPDeflate = "1.2.840.10008.1.2.4.95"
	RLELossless = "1.2.840.10008.1.2.5"
	RFC2557     = "1.2.840.10008.1.2.6.1"

	MPEG2   = "1.2.840.10008.1.2.4.100"
	MPEG4   = "1.2.840.10008.1.2.4.102"
	MPEG4BD = "1.2.840.10008.1.2.4.103"
)
