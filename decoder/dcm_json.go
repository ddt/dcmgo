package decoder

type DicomJson struct {
	Elements             []*DicomElement `json:"elements"`
	Photometric          string          `json:"-"`
	Rows                 int             `json:"-"`
	Columns              int             `json:"-"`
	WindowCenter         int             `json:"-"`
	WindowWidth          int             `json:"-"`
	BitsAllocated        int             `json:"-"`
	BitsStored           int             `json:"-"`
	HighBit              int             `json:"-"`
	PixelRepresentation  int             `json:"-"`
	PixelData            []byte          `json:"-"`
	LutNormal            []byte          `json:"-"`
	TransferSyntax       string          `json:"-"`
	SamplePerPixel       int             `json:"-"`
	PlannerConfiguration int             `json:"-"`
	NumberOfFrames       int             `json:"-"`
	isBigEndian          bool            `json:"-"`
}

type DicomElement struct {
	Tag   []string        `json:"tag"`
	VR    string          `json:"vr"`
	VL    int             `json:"vl"`
	VF    string          `json:"vf"`
	Data  []byte          `json:"-"`
	Items []*DicomElement `json:"items"`
}

func newDicomJson() *DicomJson {
	return &DicomJson{}
}

func newDicomElement() *DicomElement {
	return &DicomElement{}
}

func (this *DicomJson) SetElements(elements []*DicomElement) {
	this.Elements = elements
}
func (this *DicomJson) AddElement(element *DicomElement) {
	this.Elements = append(this.Elements, element)
}

func (this *DicomElement) AddItems(items *DicomElement) {
	this.Items = append(this.Items, items)
}
func (this *DicomElement) AddItem(item *DicomElement) {
	var it *DicomElement
	length := len(this.Items)
	if length == 0 {
		it = newDicomElement()
	} else {
		it = this.Items[length-1]
		if length == 1 {
			this.Items = nil
		} else {
			this.Items = this.Items[:length-1]
		}
	}
	it.Items = append(it.Items, item)
	this.AddItems(it)
}
