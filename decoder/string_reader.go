package decoder

import "github.com/axgle/mahonia"

type StringReader struct {
}

func (this *StringReader) GetString(str, charset string) string {
	if charset == "" {
		return str
	}
	dec := mahonia.NewDecoder(charset)
	return dec.ConvertString(str)

}
