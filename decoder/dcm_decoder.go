package decoder

type DicomDecoder struct {
	dcmEntry *DcmEntry
}

func NewDicomDecoder() *DicomDecoder {
	dcmEntry := newDcmEntry()
	return &DicomDecoder{dcmEntry}
}

func (this DicomDecoder) Decode(dcmName string) ([]byte, *DcmEntry, error) {
	return this.dcmEntry.readDcm(dcmName, false, true, "", "")
}
