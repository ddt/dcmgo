package img

import (
	"dcmgo/cost"
)

func GetCompression(data []byte, transferSyntax string) []byte {
	switch transferSyntax {
	case cost.ImplictVREndian, cost.ExplictVRBigEndian, cost.ExplictVRLittleEndian,cost.DeflatedExplictVRLittleEndian:
		return data
	case cost.JPEGLSLossless:
		return jpegLossless(data)
	default:
		return data
	}
}

func jpegLossless(data []byte) []byte {
	return data
}
