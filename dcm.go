package main

import (
	"dcmgo/decoder"
)

type DCM struct {
	cmd *Cmd
	dcmDecoder *decoder.DicomDecoder
}

func newDCM(cmd *Cmd) *DCM {
	dcmDecoder := decoder.NewDicomDecoder()
	return &DCM {
		cmd:cmd,
		dcmDecoder:dcmDecoder,
	}
}

func (this *DCM) start()  {
	this.dcmDecoder.Decode(this.cmd.file)
}