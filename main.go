package main

import (
	"time"
	"fmt"
)

func main()  {
	cmd := parseCmd()

	if cmd.versionFlag {
		println("dcmgo version: \"0.0.1\"\ndaidetian@gmail.com")
	} else if cmd.helpFlag || cmd.file == "" {
		printUsage()
	} else {
		begin := time.Now()
		newDCM(cmd).start()
		fmt.Printf("\ncost time %v ", time.Since(begin))
	}
}
